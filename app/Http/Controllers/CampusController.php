<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AdminCampusRequest;
use App\Repositories\CampusRepository;

class CampusController extends Controller
{
    private $repository;

    public function __construct(CampusRepository $campusRepository)
    {
        $this->repository = $campusRepository;
    }

    public function index()
    {
        return view('pages.campus.index');
    }

    public function getData()
    {
        $query = $this->repository->all();
        return Response::json($query);
    }

    public function getCampus($id)
    {
        $query = $this->repository->find($id);
        return Response::json($query);
    }

    public function save(AdminCampusRequest $request)
    {
        $data = $request->all();
        if ($data['id_campus']>0)
        {
            try{
                $this->repository->update($request->all(), $data['id_campus']);
            } catch ( \Illuminate\Database\QueryException $e)
            {
                return Response::json(array(
                    'success'   =>'false',
                    'msg'       =>'Falha ao atualizer registro!'));
            }
            return Response::json(array(
                'success'   =>'true',
                'msg'       =>'Registro alterado com sucesso!'
            ));

        }else{
            try{
                $this->repository->create($data);
            } catch ( \Illuminate\Database\QueryException $e)
            {
                return Response::json(array(
                    'success'   =>'false',
                    'msg'       =>'Falha ao criar registro!'
                ));
            }
            return Response::json(array(
                'success'   =>'true',
                'msg'       =>'Registro armazenado com sucesso!'
            ));
        }
    }

    public function delete($id )
    {
        try {
            $this->repository->delete($id );
        } catch ( \Illuminate\Database\QueryException $e)
        {
            if ($e->errorInfo[0]=='23000')
                return Response::json(array(
                    'success'   =>'false',
                    'msg'       =>'Este registro não pode ser removido pois está associado a uma solicitação!'
                ));
            else
                return Response::json(array(
                    'success'   =>'false',
                    'msg'       =>'Falha ao remover registro!'
                ));
        }
        return Response::json(array(
            'success'   =>'true',
            'msg'       =>'Registro removido com sucesso!'
        ));
    }

}
