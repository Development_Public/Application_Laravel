<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


class Campus extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'campus';
    protected $primaryKey = 'id_campus';

    protected $fillable = [
        'campus',
        'tel',
        'cep',
        'endereco',
        'uf',
        'cidade',
        'bairro',
        'logradouro',
        'complemento'
    ];

}
