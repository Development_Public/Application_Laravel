<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CampusRepository;
use App\Models\Campus;


/**
 * Class CampusRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CampusRepositoryEloquent extends BaseRepository implements CampusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Campus::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    public function getPage($limit,$offset,$field,$direction,$search){

        if ($search!=null){
            return ['data'=>$this->model->search($search)->limit($limit)->offset($offset)->orderby($field,$direction)->get(),'total'=>$this->model->search($search)->get()->count()];
        }
        else
            return ['data'=>$this->model->limit($limit)->offset($offset)->orderby($field,$direction)->get(),'total'=>$this->model->count()];
    }
}
