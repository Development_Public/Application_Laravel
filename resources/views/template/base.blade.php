<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>AU |  @yield('title')</title>

    <link rel="icon" type="image/x-icon" href="{{ asset('images/logoSimbolo.png') }}" />
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/materialize.css') }}" media="screen,projection">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" media="screen,projection">
   
    @yield('css')

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/materialize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    @yield('js')

</head>
    <body>

    @include('template.menu')
    <nav>
        <div class="nav-wrapper cor-default">
            <a  href            ="#"
                data-activates  ="mobile-demo"
                class           ="button-collapse">
                <i class="material-icons">menu</i>
            </a>

            <a href="/" class="brand-logo">
                AU
            </a>

            <span class="pagina-atual hide-on-med-and-down">
                    @yield('page')
            </span>

            <span class="pagina-atual-mobile right hide-on-large-only">
                    @yield('page')
            </span>

            <ul  class="right hide-on-med-and-down">
                <li class="navbar-img">
                    <a  class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position:relative; padding-left:50px;">
                        <img src="{{ route('profile.image')}}" style="width:40px; height:40px; position:absolute; top:14px; left: 10px; border-radius:50%">
                        <span class="caret"></span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    @yield('content')
    </body>
</html>
