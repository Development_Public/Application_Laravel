
<ul class="side-nav black-text" id="mobile-demo">
    <li>
        <div class="profile">
            <img  class="responsive-img circle img-profile" src="">
        </div>
    </li>

    <li>
        <ul class="collapsible collapsible-accordion">

            <li id="menu-usuario">
                <a id="name-profile" class="collapsible-header waves-effect bold ">
                    <i class="material-icons right">add</i>
                    <span class="name"> {{ Auth::user()->name }}</span>
                </a>

                <div class="collapsible-body" style="">

                    <ul>
                        <li>
                            <a href="/profile" class="waves-effect waves-light modal-trigger">

                                <i class="material-icons left">perm_identity</i>
                                 Profile User
                            </a>
                        </li>

                        <li>
                            <a href="#" class="waves-effect waves-light modal-trigger" >

                                <i class="material-icons left">settings</i>
                                Reset Password
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="material-icons left">power_settings_new</i>
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </li>

            <li>
                <a href="/" class="collapsible-header waves-effect bold ">
                    <i class="material-icons left">dashboard</i>
                    Dashboard
                </a>
            </li>
        </ul>
    </li>
</ul>

