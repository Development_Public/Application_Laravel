@extends('template.base')

@section('title')
    User Profile
@stop

@section('page')
    User Profile
@stop

@section('content')

    <div class="row">
        <div class="col s12 m4 l4">
            <div class="card  darken-1 hoverable">
                <div class="card-content white-text">
                    <div class="card-image">
                        <img src="{{ route('profile.image')}}">
                    </div>
                </div>
            </div>
        </div>

        <form enctype="multipart/form-data" action="/profile" method="POST">
            <div class="col s12 m8 l8">
                <div class="card  darken-1 hoverable">
                    <div class="card-content white-text">

                        <div class="row">
                            <div class="col s12 m6 l6">
                                <label for="upload">Upload Image</label>
                                <input class="hide" type="file" name="avatar" id="input-file-id">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input class="hide" id="input-file-id" multiple type="file" />
                                <label for="input-file-id" class="btn waves-effect waves-light col s12">Upload Image</label>
                            </div>

                            <div class="col s12 m6 l6">
                                <label for="submit">To Send</label>
                                <button type="submit" class="btn waves-effect waves-light col s12">
                                    To Send
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>

        <div class="col s12 m8">
            <div class="card  darken-1 hoverable">
                <div class="card-content">
                    <div class="row">
                        <div class="input-field col s12 m6 l6">
                            <input id="first_name" type="text" class="validate" value="{{ Auth::user()->name }}">
                            <label for="first_name">First Name</label>
                        </div>

                        <div class="input-field col s12 m6 l6">
                            <input id="email" type="email" class="validate" value="{{ Auth::user()->email }}">
                            <label for="email">Email</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
