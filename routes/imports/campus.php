<?php

    Route::get('/campus', [

        'as'                  => 'campus',
        'uses'                => 'CampusController@index'
    ]);

    Route::post('/campus/save/', [

        'as'                  => 'campus.save',
        'uses'                => 'CampusController@save'
    ]);

    Route::get('/campus/delete/{id}', [

        'as'                  => 'campus.delete',
        'uses'                => 'CampusController@delete'
    ]);

    Route::get('/campus/get-campus/{id}', [

        'as'                 => 'campus.get_campus',
        'uses'               => 'CampusController@getCampus'
    ]);

    Route::get('/campus/getData', [

        'as'                 => 'campus.getData',
        'uses'               => 'CampusController@getData'
    ]);


