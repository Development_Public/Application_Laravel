<?php

/*Route the Image*/
Route::get('profile', 'ImageUploadController@profile');

Route::post('profile', 'ImageUploadController@updateAvatar');

Route::get('/userimage', [
    'uses' => 'ImageUploadController@getUserImage',
    'as'   => 'profile.image'
]);
