<?php

use Illuminate\Database\Seeder;

class CampusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('campus')->insert([
            'campus'                          =>'Asa Sul',
            'tel'                             =>'4333333333',
            'cep'                             =>'70000000',
            'endereco'                        =>'Águas Claras',
            'uf'                              =>'DF',
            'cidade'                          =>'Brasilia',
            'bairro'                          =>'Sul',
            'logradouro'                      =>'Rua 36',
            'complemento'                     =>'Igreja',
        ]);

    }
}
