<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {

        DB::table('users')->delete();

        DB::table('users')->insert([
            'name'=>'Administrator',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('123456'),
        ]);
    }

}
